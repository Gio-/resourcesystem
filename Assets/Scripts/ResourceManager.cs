﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    [SerializeField]
    private List<Resource> m_resources = new List<Resource>();

    public void AddResource(Resource resource)
    {
        if(m_resources == null)
            return;
        
        m_resources.Add(resource);
    }

    public void RemoveResource(Resource resource)
    {
        if(m_resources == null || m_resources.Count == 0) 
            return;
        
        m_resources.Remove(resource);
    }

    public Resource GetResourceFromID(ResourceID resID)
    {
        int index = m_resources.FindIndex((x)=>x.ResourceID == resID);

        if(index < 0)
            return null;

        return m_resources[index];
    }

    public void SetResourceValue(ResourceID resID, float value)
    {
        Resource res = GetResourceFromID(resID);

        if(res != null)
            res.BaseValue = value;
    }
    
    public float GetResourceValue(ResourceID resID)
    {
        Resource res = GetResourceFromID(resID);

        if(res != null)
            return res.BaseValue;

        return -1;
    }
}
