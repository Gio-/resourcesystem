﻿
/// <summary>
/// Write down here the when the resource modifier will
/// be effective
/// </summary>
public enum ResourceModifierCondition
{
    EVER,
    IN_TRAVEL,
    IN_COMBACT,
    HIGH_FEAR,
    LOW_HEALTH,
    HIGH_HEALTH

    // Add here
}