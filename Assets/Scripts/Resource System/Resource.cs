﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

[System.Serializable]
public class Resource 
{
    [UnityEngine.SerializeField]
    private ResourceID              m_resourceID    = default(ResourceID);

    [UnityEngine.SerializeField]
    private FloatReference          m_baseValue     = null;
    
    [UnityEngine.SerializeField]
    private List<ResourceModifier>  m_modifiers;

    //private bool                    m_isDirty;

    public ReadOnlyCollection<ResourceModifier> Modifiers;
    public ResourceID   ResourceID      
    { 
        get => m_resourceID; 
    }
    public float        BaseValue       
    { 
        get => m_baseValue.Value;  
        set => m_baseValue.Value = value; 
    }
  
    public Resource(float baseValue)
	{
		m_baseValue.Value = baseValue;
        //m_isDirty         = false;
		m_modifiers       = new List<ResourceModifier>();
        Modifiers         = m_modifiers.AsReadOnly();
	}

    public void AddModifier(ResourceModifier mod)
    {
        //m_isDirty = true;
        m_modifiers.Add(mod);
        m_modifiers.Sort(CompareModifierOrder);
    }

    public bool RemoveModifier(ResourceModifier mod)
    {
        return m_modifiers.Remove(mod);
    }

    public bool RemoveAllModifiersFromSource(object source)
    {
        bool didRemove = false;

        for (int i = m_modifiers.Count - 1; i >= 0; i--)
        {
            if (m_modifiers[i].Source == source)
            {
                //m_isDirty = true;
                didRemove = true;
                m_modifiers.RemoveAt(i);
            }
        }
        return didRemove;
    }

    public float GetValueWithModifier(float value, ResourceModifierCondition condition)
    {
        /// Select all modifier with right condition
        List<ResourceModifier> modifiers = m_modifiers.FindAll((x)=>x.Condition == condition);

        if(modifiers == null || modifiers.Count == 0)
            return value;

        /// If something is found. Then calculate the final value.
        float finalValue    = value;
        float sumPercentAdd = 0; // This will hold the sum of our "PercentAdd" modifiers

        ResourceModifier mod = null;
        for (int i = 0; i < modifiers.Count; i++)
        {
            mod = modifiers[i];

            if (mod.Type == ResourceModifierType.Flat)
            {
                finalValue += mod.Value;
            }
            else if (mod.Type == ResourceModifierType.PercentAdd) // When we encounter a "PercentAdd" modifier
            {
                sumPercentAdd += mod.Value; // Start adding together all modifiers of this type

                // If we're at the end of the list OR the next modifer isn't of this type
                if (i + 1 >= modifiers.Count || modifiers[i + 1].Type != ResourceModifierType.PercentAdd)
                {
                    finalValue *= 1 + sumPercentAdd; // Multiply the sum with the "finalValue", like we do for "PercentMult" modifiers
                    sumPercentAdd = 0; // Reset the sum back to 0
                }
            }
            else if (mod.Type == ResourceModifierType.PercentMult) // Percent renamed to PercentMult
            {
                finalValue *= 1 + mod.Value;
            }
        }

        return (float)Math.Round(finalValue, 4);
    }

    private int CompareModifierOrder(ResourceModifier a, ResourceModifier b)
    {
        if (a.Order < b.Order)
            return -1;
        else if (a.Order > b.Order)
            return 1;
        return 0; // if (a.Order == b.Order)
    }
}
