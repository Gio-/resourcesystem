﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceModifierType
{
    Flat        = 100,
    PercentAdd  = 200,
    PercentMult = 300
}

public class ResourceModifier 
{
    public readonly float Value;
    public readonly ResourceModifierType Type;
    public readonly ResourceModifierCondition Condition;
    public readonly int Order;
    public readonly object Source;

    public ResourceModifier(float value, ResourceModifierType type, ResourceModifierCondition condition, int order, object source)
    {
        Value       = value;
        Type        = type;
        Condition   = condition;
        Order       = order;
        Source      = source;
    }

        
    // Requires Value and Type. Calls the "Main" constructor and sets Order and Source to their default values: (int)type and null, respectively.
    public ResourceModifier(float value, ResourceModifierType type) : this(value, type, ResourceModifierCondition.EVER, (int)type, null) { }

    // Requires Value, Type and Order. Sets Source to its default value: null
    public ResourceModifier(float value, ResourceModifierType type, int order) : this(value, type, ResourceModifierCondition.EVER, order, null) { }

    // Requires Value, Type and Source. Sets Order to its default value: (int)Type
    public ResourceModifier(float value, ResourceModifierType type, object source) : this(value, type, ResourceModifierCondition.EVER, (int)type, source) { }

    // Requires Value, Type and Condition. Sets Order to its default value: (int)Type and Source to null
    public ResourceModifier(float value, ResourceModifierType type, ResourceModifierCondition condition) : this(value, type, condition, (int)type, null) { }

    // Requires Value, Type, Condition and Order. Sets Source to its default value: null
    public ResourceModifier(float value, ResourceModifierType type, ResourceModifierCondition condition, int order) : this(value, type, condition, order, null) { }

    // Requires Value, Type, Condition and Source. Sets Order to its default value: (int)Type
    public ResourceModifier(float value, ResourceModifierType type, ResourceModifierCondition condition, object source) : this(value, type, condition, (int)type, source) { }
    

}
