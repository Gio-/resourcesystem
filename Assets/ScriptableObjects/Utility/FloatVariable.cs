﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FloatVariable : ScriptableObject 
{
	[SerializeField]
	private float m_baseValue = 0.0f;

	[ReadOnlyAttribute]
	[SerializeField]
	private float m_value;
    
	public float value 
	{ 
		get => m_value;
		set => m_value = value;
	} 
	
	public void OnEnable()
	{
		value = m_baseValue;	
	}
}
